# An implementation of a possible simple random sampling based on the lottery principle, where a certain amount of people will be equally distributed among two groups
#
# by Alucard17, alucard17@gitlab
#

from random import *

# the user inputs the number of people participating
population = int(input("Sample size: "))

# a list from 1 to the number of people is created i.e. [1,2,3,4] for 4 people
mylist = list(range(population))
for i in mylist:
    mylist[i] = i+1

# this is used for odd numbers to choose if list A or B has one more member
tmp = randint(0,1)

# the core of the code, it decides if the population is even or odd. If it is odd, then the avove random variable decides, which list will be longer than the other. If the population is even, this step will be skipped. In either case a cetrain sample will be randomly choosen from the original list, precisely half of the population, and added to group A. The rest is then placed in group B.
if population % 2 == 1:
    if tmp == 0:
        count = int((population - 1)/2)
        A = sample(mylist, count)
        B = mylist
        for i in A:
            if i in B:
                B.remove(i)
    else:
        count = int((population - 1)/2+1)
        A = sample(mylist, count)
        B = mylist
        for i in A:
            if i in B:
                B.remove(i)

else:
    count = int((population)/2)
    A = sample(mylist, count)
    B = mylist
    for i in A:
        if i in B:
            B.remove(i)

# The final output of the code. It gives the number of population, and which candidate is chosen in which group.
print("-------------------------------")
print("Sample size: " + str(population))
print("-------------------------------")
print("Members in list A: ")
print(A)
print("-------------------------------")
print("Members in list B: ")
print(B)
print("-------------------------------")
